#!/bin/bash

curl -vvvv \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"GitLab":"FaaS"}' \
  http://functions-echo-js.serverless-webcast-15423787-production.knative.eggshell.me
